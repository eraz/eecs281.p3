#include "log.h"

using namespace std;

Log::Log(size_t i_, string t_, string c_, string m_)
: i(i_), t(0), c(c_) {
    for (size_t i = 0, count = 9; i < t_.size(); ++i) {
        size_t pow = 1;
        for (size_t i = 0; i < count; ++i) pow *= 10;
        if (isdigit(t_[i])) {
            t += (t_[i] - '0') * pow;
            --count;
        }
    }

    e = to_string(i) + '|' + t_ + '|' + c_ + '|' + m_;
}
