#include <algorithm>
#include <limits>
#include <sstream>
#include <queue>

#include "imode.h"

using namespace std;

inline void parse(Log* log, string s, svmap& map) {
    string word;

    for (char c : s) {
        if (isalnum(c)) word.push_back(c);
        else {
            if (map.find(word) == map.end()) {
                map.emplace(word, move(lvector(1, log)));
            } else {
                if (map[word].back() != log) map[word].push_back(log);
            }
            word.clear();
        }
    }

    if (!word.empty()) {
        if (map.find(word) == map.end()) {
            map.emplace(word, move(lvector(1, log)));
        } else {
            if (map[word].back() != log) map[word].push_back(log);
        }
    }
}

void initialize(lvector& logs, svmap& cat, svmap& words) {
    sort(logs.begin(), logs.end(), Log::comp());

    for (Log* log : logs) {
        string c = log->category();
        string m = log->entry().substr(log->entry().find_last_of('|'));

        transform(c.begin(), c.end(), c.begin(), ::tolower);
        transform(m.begin(), m.end(), m.begin(), ::tolower);

        if (cat.find(c) == cat.end()) {
            cat.emplace(c, move(lvector(1, log)));
        } else {
            cat[c].push_back(log);
        }

        c.append(' ' + move(m));

        transform(c.begin(), c.end(), c.begin(), [](char c) {
            return (isalnum(c) == 0) ? ' ' : c;
        });

        parse(log, move(c), words);
    }
}

lvector* mass_intersection(vector<lvector*> lists) {
    lvector* output = new lvector;

    vector<lvector::iterator> iters;
    for (lvector* list : lists) iters.push_back(list->begin());

    size_t max = 0;
    bool running = true;

    while (running) {
        size_t count = 1;
        Log::comp comp;

        for (size_t i = 0; count < iters.size(); i = (i + 1) % iters.size()) {
            if (i == max) continue;

            // For current iter, increment until not less.
            while ( iters[i] != lists[i]->end()
                    &&
                    comp(*iters[i], *iters[max])
                    ) ++iters[i];

            // End intersection when a list has been completed.
            if (iters[i] == lists[i]->end()) {
                running = false;
                break;
            }

            // If new iter is greater, set as max and reset count.
            if (comp(*iters[max], *iters[i])) {
                ++iters[max];
                max = i;
                count = 1;
            // If equal, increment count and continue search.
            } else {
                ++count;
            }
        }

        if (!running) break;

        output->push_back(*iters[max]);

        for (lvector::iterator& i : iters) ++i;

        if (iters[max] == lists[max]->end()) break;
    }

    return output;
}

struct Error {};

void imode(lvector logs) {
#ifdef NDEBUG
    ostringstream oss;
    ostream& os = oss;
#else
    ostream& os = cout;
#endif

    lvector id = logs;
    svmap cat;
    svmap words;

    initialize(logs, cat, words);

    ldeque excerpt;
    lvector* buffer = nullptr;
    size_t bbegin = 0;
    size_t bend = 0;
    char command;
    bool delbuf = false;
    enum State { NONE, TIMESTAMP, CATEGORY, KEYWORD } state = NONE;

    do {
        os   << "% ";
        cin >> command;

        try {
            switch (command) {
            // ================================================================
            // Search commands.
            // ================================================================
            case 't':   os  << t(logs, bbegin, bend)
                            << " entries found\n";
                        state = TIMESTAMP;
                        break;

            case 'c':   os  << c(cat, buffer, delbuf)
                            << " entries found\n";
                        state = CATEGORY;
                        break;

            case 'k':   os  << k(words, buffer, delbuf) << " entries found\n";
                        state = KEYWORD;
                        break;

            // ================================================================
            // Excerpt editing commands.
            // ================================================================
            case 'a':   os      << "log entry " << a(id, excerpt)
                                << " appended\n";
                        break;

            case 'r':   if (state == NONE) throw Error();
                        if (state == TIMESTAMP) {
                            for (size_t i = bbegin; i < bend; ++i) {
                                excerpt.push_back(logs[i]);
                            }

                            os  << (bend - bbegin)
                                << " log entries appended\n";
                        } else {
                            for (Log* log : *buffer) excerpt.push_back(log);
                            os << buffer->size() << " log entries appended\n";
                        }
                        break;

            case 'd':   os  << "excerpt list entry " << d(excerpt)
                            << " deleted\n";
                        break;

            case 'b':   os  << "excerpt list entry " << b(excerpt)
                            << " moved\n";
                        break;

            case 'e':   os  << "excerpt list entry " << e(excerpt)
                            << " moved\n";
                        break;

            case 's':   sort(excerpt.begin(), excerpt.end(), Log::comp());
                        os  << "excerpt list sorted\n";
                        break;

            case 'l':   excerpt.clear();
                        os  << "excerpt list cleared\n";
                        break;

            // ================================================================
            // Output commands.
            // ================================================================
            case 'g':   if (state == NONE) throw Error();
                        if (state == TIMESTAMP) {
                            for (size_t i = bbegin; i < bend; ++i) {
                                os << logs[i]->entry() << '\n';
                            }
                        } else {
                            for (Log* log : *buffer) os << log->entry() << '\n';
                        }
                        break;

            case 'p':   p(excerpt, os);
                        break;

            // ================================================================
            // Miscellaneous commands.
            // ================================================================
            case 'q':   break;

            case '#':   cin.ignore(numeric_limits<streamsize>::max(), '\n');
                        break;

            default:    cerr << "Error: Invalid command\n";
            }
        } catch (Error& e) {
            cerr << "Error: Invalid command\n";
        }
    } while (command != 'q');

#ifdef NDEBUG
    cout << move(oss.str());
#endif

    if (delbuf) delete buffer;
    for (Log* log : logs) delete log;
}

inline size_t t(lvector& logs, size_t& bbegin, size_t& bend) {
    string time1, time2;
    cin.ignore();
    getline(cin, time1, '|');
    getline(cin, time2);

    if (time1.size() != 14 || time2.size() != 14) throw Error();

    unsigned long t1 = Log::convert(time1);
    unsigned long t2 = Log::convert(time2);

    bbegin = distance(logs.begin(), lower_bound(
            logs.begin(),
            logs.end(),
            t1,
            [](Log* log, const unsigned long t1) {
                return log->timestamp() < t1;
            }));

    bend =  distance(logs.begin(), upper_bound(
            logs.begin(),
            logs.end(),
            t2,
            [](const unsigned long t2, Log* log) {
                return t2 <= log->timestamp();
            }));

    return bend - bbegin;
}

inline size_t c(svmap& cat, lvector*& buffer, bool& delbuf) {
    string input;
    cin.ignore();
    getline(cin, input);

    transform(input.begin(), input.end(), input.begin(), ::tolower);

    if (delbuf) delete buffer;

    if (cat.find(input) == cat.end()) {
        buffer = new lvector;
        delbuf = true;
    } else {
        buffer = &cat[input];
        delbuf = false;
    }

    return buffer->size();
}

inline size_t k(svmap& words, lvector*& buffer, bool& delbuf) {
    if (delbuf) delete buffer;
    delbuf = true;

    string input;
    cin.ignore();
    getline(cin, input);

    transform(input.begin(), input.end(), input.begin(), ::tolower);

    vector<string> keywords;
    string word;

    for (char c : input) {
        if (isalnum(c)) word.push_back(c);
        else {
            keywords.emplace_back(move(word));
            word.clear();
        }
    }

    if (!word.empty()) keywords.emplace_back(move(word));

    sort(keywords.begin(), keywords.end());
    keywords.resize(distance(   keywords.begin(),
                                unique_copy(keywords.begin(),
                                            keywords.end(),
                                            keywords.begin()
                                            )
                                ));

    vector<lvector*> lists;

    for (string& str : keywords) {
        if (words.find(str) != words.end()) {
            lists.emplace_back(&words[move(str)]);
        } else {
            buffer = new lvector;
            return 0;
        }
    }

    if (lists.size() == 1) buffer = new lvector(*lists[0]);
    else buffer = mass_intersection(move(lists));

    return buffer->size();
}

inline size_t a(lvector& id, ldeque& excerpt) {
    size_t index;
    cin.ignore();
    cin >> index;

    if (index >= id.size()) throw Error();

    excerpt.emplace_back(id[index]);

    return index;
}

inline size_t d(ldeque& excerpt) {
    size_t index;
    cin.ignore();
    cin >> index;

    if (index >= excerpt.size()) throw Error();

    excerpt.erase(next(excerpt.begin(), index));

    return index;
}

inline size_t b(ldeque& excerpt) {
    size_t index;
    cin.ignore();
    cin >> index;

    if (index >= excerpt.size()) throw Error();

    ldeque::iterator i = next(excerpt.begin(), index);
    Log* log = *i;
    excerpt.erase(i);

    excerpt.emplace_front(log);

    return index;
}

inline size_t e(ldeque& excerpt) {
    size_t index;
    cin.ignore();
    cin >> index;

    if (index >= excerpt.size()) throw Error();

    ldeque::iterator i = next(excerpt.begin(), index);
    Log* log = *i;
    excerpt.erase(i);

    excerpt.emplace_back(log);

    return index;
}

inline void p(ldeque& excerpt, ostream& os) {
    size_t count = 0;

    for (Log* log : excerpt) os << count++ << '|' << log->entry() << '\n';
}
