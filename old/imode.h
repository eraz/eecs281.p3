#ifndef IMODE_H
#define IMODE_H

#include <deque>
#include <iostream>
#include <string>
#include <unordered_map>
#include <vector>

#include "log.h"

typedef std::vector<Log*> lvector;
typedef std::deque<Log*> ldeque;
typedef std::unordered_map<std::string, lvector> svmap;

void imode(lvector logs);

inline size_t t(lvector& logs, size_t& bbegin, size_t& bend);
inline size_t c(svmap& cat, lvector*& buffer, bool& delbuf);
inline size_t k(svmap& words, lvector*& buffer, bool& delbuf);
inline size_t a(lvector& id, ldeque& excerpt);
inline size_t d(ldeque& excerpt);
inline size_t b(ldeque& excerpt);
inline size_t e(ldeque& excerpt);
inline void p(ldeque& excerpt, std::ostream& os);

#endif
