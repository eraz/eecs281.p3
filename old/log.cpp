#include <cmath>

#include "log.h"

using namespace std;

Log::Log(size_t i_, string t_, string c_, string m_)
: i(i_), t(0), c(c_), e("") {
    t = convert(t_);

    e.append(to_string(i_) + '|' + move(t_) + '|' + move(c_) + '|' + move(m_));
}
