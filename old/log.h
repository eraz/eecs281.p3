#ifndef LOG_H
#define LOG_H

#include <string>

class Log {
    size_t i;
    unsigned long t;
    std::string c;
    std::string e;

public:
    Log(size_t i_, std::string t_, std::string c_, std::string m_);

    static inline unsigned long convert(std::string time_) {
        unsigned long time = 0;

        for (size_t i = 0, count = 9; i < time_.size(); ++i) {
            if (isdigit(time_[i])) time += (time_[i] - '0') * pow(10, count--);
        }

        return std::move(time);
    }

    unsigned long const& timestamp() const { return t; }
    std::string const& category() const { return c; }
    std::string const& entry() const { return e; }

    struct comp {
        inline bool operator()(Log* a, Log* b) {
            return  a->t < b->t ? true  :
                    a->t > b->t ? false :
                    a->c < b->c ? true  :
                    a->c > b->c ? false :
                    a->i < b->i;
        }
    };
};

#endif
