#include <algorithm>
#include <cctype>
#include <iostream>
#include <limits>
#include <queue>
#include <sstream>
#include "imode.h"
#include "log.h"

using namespace std;

struct Error {};

void initialize(lvector& logs, lumap& cat, lumap& words) {
    sort(logs.begin(), logs.end(), Log::comp());

    for (size_t i = 0; i < logs.size(); ++i) {
        Log* log = logs[i];
        const string& c_ = log->category();
        const char* m = log->entry().data() + c_.size() + 16;
        size_t c_size = c_.size();
        size_t m_size = log->entry().size() - c_.size() - 16;

        string str; str.reserve(c_size + m_size + 1); // Extra ' '.
        string c; c.reserve(c_size);

        for (size_t j = 0; j < c_size; ++j) {
            char letter = tolower(c_[j]);
            c.push_back(letter);
            str.push_back(isalnum(letter) ? letter : ' ');
        }

        if (cat.find(c) == cat.end()) cat.emplace(c, lvector(1, log));
        else                          cat[c].push_back(log);

        str.push_back(' ');
        for (size_t j = 0; j < m_size; ++j) {
            char letter = tolower(*(m++));
            str.push_back(isalnum(letter) ? letter : ' ');
        }

        istringstream is(move(str));
        string word;
        while (is >> word) {
            if (words.find(word) == words.end()) {
                words.emplace(word, lvector(1, log));
            } else {
                if (words[word].back() != log) words[word].push_back(log);
            }
        }
    }
}

inline unsigned long convert(string t_) {
    unsigned long t = 0;

    for (size_t i = 0, count = 9; i < t_.size(); ++i) {
        size_t pow = 1;
        for (size_t i = 0; i < count; ++i) pow *= 10;
        if (isdigit(t_[i])) {
            t += (t_[i] - '0') * pow;
            --count;
        }
    }

    return t;
}

size_t t(lvector& logs, size_t& front, size_t& back) {
    string time1, time2;
    cin.ignore();
    getline(cin, time1, '|');
    getline(cin, time2);

    if (time1.size() != 14 || time2.size() != 14) throw Error();

    unsigned long   t1 = convert(move(time1)),
                    t2 = convert(move(time2));

    lvector::iterator begin = logs.begin();

    lvector::iterator middle = lower_bound(begin, logs.end(), t1,
            [](Log* log, const unsigned long& t) {
                return log->timestamp() < t;
            });

    lvector::iterator end = lower_bound(middle, logs.end(), t2,
            [](Log* log, const unsigned long& t) {
                return log->timestamp() < t;
            });

    front   = middle - begin;
    back    = end - begin;

    return back - front;
}

size_t c(lumap& cat, lvector*& buffer, bool& delbuf) {
    if (delbuf) delete buffer;

    string input;
    cin.ignore();
    getline(cin, input);

    transform(input.begin(), input.end(), input.begin(), ::tolower);

    if (cat.find(input) == cat.end()) {
        buffer = new lvector;
        delbuf = true;
    } else {
        buffer = &cat[input];
        delbuf = false;
    }

    return buffer->size();
}

size_t k(lumap& words, lvector*& buffer, bool& delbuf) {
    if (delbuf) delete buffer;

    string input;
    cin.ignore();
    getline(cin, input);

    for (size_t i = 0; i < input.size(); ++i) {
        char letter = tolower(input[i]);
        input[i] = isalnum(letter) ? letter : ' ';
    }

    vector<string> matches;

    istringstream is(input);
    string word;

    while (is >> word) {
        if (words.find(word) == words.end()) {
            buffer = new lvector;
            return 0;
        }

        matches.emplace_back(move(word));
    }

    if (matches.size() == 1) {
        buffer = &words[matches[0]];
        delbuf = false;
    } else {
        buffer = new lvector(words[matches[0]]);

        for (size_t i = 1; i < matches.size(); ++i) {
            buffer->resize(distance(buffer->begin(), set_intersection(
                    buffer->begin(),
                    buffer->end(),
                    words[matches[i]].begin(),
                    words[matches[i]].end(),
                    buffer->begin(),
                    Log::comp()
                    )));

            if (buffer->empty()) break;
        }

        delbuf = true;
    }

    return buffer->size();
}

size_t a(lvector& id, ldeque& excerpt) {
    size_t index;
    cin >> index;

    if (index >= id.size()) throw Error();

    excerpt.emplace_back(id[index]);

    return index;
}

size_t d(ldeque& excerpt) {
    size_t index;
    cin >> index;

    if (index >= excerpt.size()) throw Error();

    excerpt.erase(next(excerpt.begin(), index));

    return index;
}

size_t b(ldeque& excerpt) {
    size_t index;
    cin >> index;

    if (index >= excerpt.size()) throw Error();

    ldeque::iterator i = next(excerpt.begin(), index);
    Log* log = *i;
    excerpt.erase(i);

    excerpt.emplace_front(log);

    return index;
}

size_t e(ldeque& excerpt) {
    size_t index;
    cin >> index;

    if (index >= excerpt.size()) throw Error();

    ldeque::iterator i = next(excerpt.begin(), index);
    Log* log = *i;
    excerpt.erase(i);

    excerpt.emplace_back(log);

    return index;
}

void imode(lvector logs) {
#ifndef DEBUG
    stringstream oss; ostream& os = oss;
#else
    ostream& os = cout;
#endif

    lvector id(logs);
    lumap cat;
    lumap words;

    initialize(logs, cat, words);

    lvector* buffer = nullptr;
    ldeque excerpt;

    size_t front, back;
    char command;
    bool no_search = true;
    bool delbuf = false;
    bool toggle_t = false;

    do {
        os << "% ";
        cin >> command;

        try {
            switch (command) {
            // ================================================================
            // Search commands.
            // ================================================================
            case 't':   no_search = false;
                        os  << t(logs, front, back) << " entries found\n";
                        toggle_t = true;
                        break;

            case 'c':   no_search = false;
                        os << c(cat, buffer, delbuf) << " entries found\n";
                        toggle_t = false;
                        break;

            case 'k':   no_search = false;
                        os << k(words, buffer, delbuf) << " entries found\n";
                        toggle_t = false;
                        break;

            // ================================================================
            // Excerpt editing commands.
            // ================================================================
            case 'a':   os  << "log entry " << a(id, excerpt)
                            << " appended\n";
                        break;

            case 'r':   if (no_search) throw Error();
                        if (toggle_t) {
                            for (size_t i = front; i < back; ++i) {
                                excerpt.emplace_back(logs[i]);
                            }
                            os << back - front << " log entries appended\n";
                        } else {
                            for (size_t i = 0; i < buffer->size(); ++i) {
                                excerpt.emplace_back(buffer->operator[](i));
                            }
                            os << buffer->size() << " log entries appended\n";
                        }
                        break;

            case 'd':   os  << "excerpt list entry " << d(excerpt)
                            << " deleted\n";
                        break;

            case 'b':   os  << "excerpt list entry " << b(excerpt)
                            << " moved\n";
                        break;

            case 'e':   os  << "excerpt list entry " << e(excerpt)
                            << " moved\n";
                        break;

            case 's':   sort(excerpt.begin(), excerpt.end(), Log::comp());
                        os << "excerpt list sorted\n";
                        break;

            case 'l':   excerpt.clear();
                        os << "excerpt list cleared\n";
                        break;

            // ================================================================
            // Output commands.
            // ================================================================
            case 'g':   if (no_search) throw Error();
                        if (toggle_t) {
                            for (size_t i = front; i < back; ++i) {
                                os  << logs[i]->entry() << '\n';
                            }
                            break;
                        } else {
                            for (size_t i = 0; i < buffer->size(); ++i) {
                                os << buffer->operator[](i)->entry() << '\n';
                            }
                            break;
                        }
                        break;

            case 'p':   for (size_t i = 0; i < excerpt.size(); ++i) {
                            os << i << '|' << excerpt[i]->entry() << '\n';
                        }
                        break;

            // ================================================================
            // Miscellaneous commands.
            // ================================================================
            case 'q':   break;

            case '#':   cin.ignore(numeric_limits<streamsize>::max(), '\n');
                        break;

            default:    cerr << "Error: Invalid command\n";
            }
        } catch (Error& e) {
            cerr << "Error: Invalid command\n";
        }
    } while (command != 'q');

    if (delbuf) delete buffer;

#ifndef DEBUG
    cout << os.rdbuf();
#endif

    for (Log* log : logs) delete log;
}
