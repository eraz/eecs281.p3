#include <cmath>
#include <fstream>
#include <iostream>
#include <string>

#include "imode.h"

using namespace std;

static const string usage = "./logman: usage: logman (LOGFILE | -h)\n";

static const string help =
        "./logman: usage: logman (LOGFILE | -h)\n"
        "\n"
        "EECS 281 - Fall 2015 - Project 3\n"
        "Log Manager\n"
        "\n"
        "Reads an input file containing log entries, then enters an\n"
        "interactive mode where timestamp, category, and keyword searches\n"
        "to construct an \"excerpt list\" of the log file. Also allows the\n"
        "user to manage and display the \"excerpt list\" to identify\n"
        "important/relevant entries in the log file.\n"
        "\n"
        "Command line options:\n"
        "   -h --help   : prints this help message.\n"
        "   LOGFILE     : name of a properly formatted logfile."
        ;

int main(int argc, char** argv) {
    ios_base::sync_with_stdio(false);

    string filename = argv[1];

    if (argc != 2) {
        // Case: Only one argument should be passed.
        cout << "Command line error.\n" << usage << endl;
        exit(1);
    }

    if (filename.compare("-h") == 0 || filename.compare("--help") == 0) {
        // Case: Help flag.
        cout << help << endl;
    } else {
        ifstream is(filename);

        if (!is.is_open()) {
            cout << filename << " does not exist.\n";
            exit(1);
        }

        string t, c, m;
        size_t i = 0;

        lvector logs;

        while (getline(is, t, '|')) {
            getline(is, c, '|');
            getline(is, m);

            logs.push_back(new Log(i++, t, c, m));
        }

        cout << i << " entries read\n";

        imode(logs);
    }

    return 0;
}
