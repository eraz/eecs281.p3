#ifndef IMODE_H
#define IMODE_H

#include <deque>
#include <string>
#include <unordered_map>
#include <vector>

#include "log.h"

typedef std::vector<Log*> lvector;
typedef std::unordered_map<std::string, lvector> lumap;
typedef std::deque<Log*> ldeque;

void imode(lvector logs);

#endif
