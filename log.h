#ifndef LOG_H
#define LOG_H

#include <string>

class Log {
    size_t i;
    unsigned long t;
    std::string c;
    std::string e;

public:
    const static size_t TIME_SIZE = 14;

    Log(size_t i_, std::string t_, std::string c_, std::string m_);

    inline unsigned long const& timestamp() const { return t; }
    inline const std::string& category() const { return c; }
    inline const std::string& entry() const { return e; }

    struct comp {
        bool operator()(Log* a, Log* b) {
            return  a->t < b->t ? true  :
                    a->t > b->t ? false :
                    a->c < b->c ? true  :
                    a->c > b->c ? false :
                    a->i < b->i;
        }
    };
};

#endif
